const Event = require('./Event');
const Venue = require('./Venue');

module.exports = {
  Event,
  Venue,
};
