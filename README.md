# The Things I've Seen

Just a hobby project I've been working on to demo various technologies using the concerts and events I've attended as a dataset.

## Development

Fire up the docker containers (one for web app, one for API):

```
docker-compose up
```

Access the web app at `http://localhost:3000/`

Hit the API at `http://localhost:8080/api`